<?php

namespace App\Loader\Strategy;

/**
 * Interface StrategyInterface
 *
 * @package App\Loader\Strategy
 */
interface StrategyLoaderInterface
{
    /**
     * @return array
     */
    public function parseData(): array;
}

