<?php

declare(strict_types=1);

namespace App\Loader\Strategy;

/**
 * Class JsonLoaderStrategyLoader
 *
 * @package App\Loader\Strategy
 */
class JsonLoaderStrategyLoader implements StrategyLoaderInterface
{
    /** @var string $filePath */
    private $filePath = __DIR__ . '../../../config/tickets.json';

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     */
    public function setFilePath(string $filePath): void
    {
        $this->filePath = $filePath;
    }

    /**
     * @return array
     */
    public function parseData(): array
    {
        return \json_decode(file_get_contents($this->getFilePath()), true);
    }
}
