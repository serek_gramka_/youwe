<?php

namespace App\Loader;

/**
 * Interface DataLoaderInterface
 *
 * @package App\Loader
 */
interface DataLoaderInterface
{
    public function getData(): array ;
}
