<?php

declare(strict_types=1);

namespace App\Loader;

use App\Loader\Strategy\StrategyLoaderInterface;
use App\Mapper\TransportMapperInterface;
use App\Model\BoardingCardInterface;

/**
 * Class DataLoader
 *
 * @package App\Loader
 */
class DataLoader implements DataLoaderInterface
{
    /** @var TransportMapperInterface $transportMapper */
    private $transportMapper;

    /** @var StrategyLoaderInterface $strategy */
    private $strategyLoader;

    /** @var BoardingCardInterface  */
    private $boardingCard;

    /**
     * DataLoader constructor.
     *
     * @param TransportMapperInterface $transportMapper
     * @param StrategyLoaderInterface $strategyLoader
     * @param BoardingCardInterface $boardingCard
     */
    public function __construct(
        TransportMapperInterface $transportMapper,
        StrategyLoaderInterface $strategyLoader,
        BoardingCardInterface $boardingCard
    ) {
        $this->transportMapper = $transportMapper;
        $this->strategyLoader = $strategyLoader;
        $this->boardingCard = $boardingCard;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $rawData = $this->strategyLoader->parseData();
        $randomizedData = $this->randomize($rawData);
        $resultData = [];

        foreach ($randomizedData as $key => $cardData) {
            $transport = $this->transportMapper->map($cardData['transport']);

            $card = $this->boardingCard->init();
            $card->setFromCity($cardData['from_place']);
            $card->setToCity($cardData['to_place']);
            $card->setSeat($cardData['seat']);
            $card->setTransport($transport);
            $card->setUid($key);

            $resultData[$card->getUid()] = $card;
        }

        return $resultData;
    }

    /**
     * Simple randomize for array
     *
     * @param array $data
     *
     * @return array
     */
    private function randomize(array $data): array
    {
        shuffle($data);

        return $data;
    }
}
