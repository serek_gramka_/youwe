<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class BoardingCard
 *
 * @package App\Model
 */
class BoardingCard implements BoardingCardInterface, UidInterface
{
    /** @var int $uid */
    private $uid;

    /** @var TransportInterface $transport */
    private $transport;

    /** @var string $fromCity */
    private $fromCity;

    /** @var string $toCity */
    private $toCity;

    /** @var string $seat */
    private $seat;

    /**
     * @return BoardingCardInterface
     */
    public function init(): BoardingCardInterface
    {
        return new self();
    }

    /**
     * @return TransportInterface
     */
    public function getTransport(): TransportInterface
    {
        return $this->transport;
    }

    /**
     * @param TransportInterface $transport
     */
    public function setTransport(TransportInterface $transport): void
    {
        $this->transport = $transport;
    }

    /**
     * @return string
     */
    public function getFromCity(): string
    {
        return $this->fromCity;
    }

    /**
     * @param string $fromCity
     */
    public function setFromCity(string $fromCity): void
    {
        $this->fromCity = $fromCity;
    }

    /**
     * @return string
     */
    public function getToCity(): string
    {
        return $this->toCity;
    }

    /**
     * @param string $toCity
     */
    public function setToCity(string $toCity): void
    {
        $this->toCity = $toCity;
    }

    /**
     * @return string
     */
    public function getSeat(): string
    {
        return $this->seat;
    }

    /**
     * @param string $seat
     */
    public function setSeat(string $seat): void
    {
        $this->seat = $seat;
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }
}
