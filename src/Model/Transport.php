<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class Transport
 *
 * @package App\Model
 */
class Transport implements TransportInterface
{
    /** @var string $number */
    private $number;

    /**
     * @param string $number
     */
    public function __construct(string $number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }
}
