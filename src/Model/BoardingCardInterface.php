<?php

namespace App\Model;

/**
 * Interface BoardingCardInterface
 *
 * @package App\Model
 */
interface BoardingCardInterface
{
    /**
     * @return BoardingCardInterface
     */
    public function init(): BoardingCardInterface;

    /**
     * @return TransportInterface
     */
    public function getTransport(): TransportInterface;

    /**
     * @param TransportInterface $transport
     */
    public function setTransport(TransportInterface $transport): void ;

    /**
     * @return string
     */
    public function getFromCity(): string;

    /**
     * @param string $fromCity
     */
    public function setFromCity(string $fromCity): void ;

    /**
     * @return string
     */
    public function getToCity(): string;

    /**
     * @param string $toCity
     */
    public function setToCity(string $toCity): void ;

    /**
     * @return string
     */
    public function getSeat(): string;

    /**
     * @param string $seat
     */
    public function setSeat(string $seat): void ;
}
