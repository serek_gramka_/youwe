<?php

namespace App\Model;

/**
 * Interface UidInterface
 *
 * @package App\Model
 */
interface UidInterface
{
    public function getUid(): int;
    public function setUid(int $uid);
}