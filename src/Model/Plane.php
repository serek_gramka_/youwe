<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class Plane
 *
 * @package App\Model
 */
class Plane extends Transport
{
    public const GATE_KEY = 'gate';
    public const BAGGAGE_KEY = 'baggage';

    /** @var string $gate */
    private $gate;

    /** @var string $baggage */
    private $baggage;

    /**
     * @param string $number
     * @param string $gate
     * @param string $baggage
     */
    public function __construct(string $number, string $gate, string $baggage)
    {
        parent::__construct($number);

        $this->gate = $gate;
        $this->baggage = $baggage;
    }

    /**
     * @return string
     */
    public function getGate(): string
    {
        return $this->gate;
    }

    /**
     * @param string $gate
     */
    public function setGate(string $gate): void
    {
        $this->gate = $gate;
    }

    /**
     * @return string
     */
    public function getBaggage(): string
    {
        return $this->baggage;
    }

    /**
     * @param string $baggage
     */
    public function setBaggage(string $baggage): void
    {
        $this->baggage = $baggage;
    }
}
