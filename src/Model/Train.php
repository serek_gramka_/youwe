<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class Train
 *
 * @package App\Model
 */
class Train extends Transport
{
    /**
     * @param string $number
     */
    public function __construct(string $number)
    {
        parent::__construct($number);
    }
}
