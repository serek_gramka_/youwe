<?php

namespace App\Model;

/**
 * Interface TransportInterface
 *
 * @package App\Model
 */
interface TransportInterface
{
    /**
     * @return string
     */
    public function getNumber(): string;

    /**
     * @param string $number
     *
     * @return mixed
     */
    public function setNumber(string $number);
}
