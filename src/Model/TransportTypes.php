<?php

namespace App\Model;

/**
 * Class TransportTypes
 *
 * @package App\Model
 */
class TransportTypes
{
    public const NUMBER_KEY = 'number';

    public const BUS = 'bus';
    public const TRAIN = 'train';
    public const PLANE = 'plane';

    public const TYPES = [
        self::BUS => self::BUS,
        self::TRAIN => self::TRAIN,
        self::PLANE => self::PLANE,
    ];
}
