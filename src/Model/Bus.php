<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class Bus
 *
 * @package App\Model
 */
class Bus extends Transport
{
    /**
     * @param string $number
     */
    public function __construct(string $number)
    {
        parent::__construct($number);
    }
}
