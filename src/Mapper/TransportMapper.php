<?php

declare(strict_types=1);

namespace App\Mapper;

use App\Model\Bus;
use App\Model\Plane;
use App\Model\Train;
use App\Model\Transport;
use App\Model\TransportInterface;
use App\Model\TransportTypes;

/**
 * Class TransportMapper
 *
 * @package App\Mapper
 */
class TransportMapper implements TransportMapperInterface
{
    /**
     * @param array $transportData
     *
     * @return Transport|null
     */
    public function map(array $transportData): ?TransportInterface
    {
        $type = $transportData['type'];
        $transport = null;

        if (!in_array($type, TransportTypes::TYPES)) {
            return null;
        }

        switch ($type) {
            case TransportTypes::BUS:
                $transport = new Bus('None');
                break;

            case TransportTypes::PLANE:
                $transport = new Plane(
                    $transportData[TransportTypes::NUMBER_KEY],
                    $transportData[Plane::GATE_KEY],
                    $transportData[Plane::BAGGAGE_KEY]
                );
                break;

            case TransportTypes::TRAIN:
                $transport = new Train($transportData[TransportTypes::NUMBER_KEY]);
                break;
        }

        return $transport;
    }
}
