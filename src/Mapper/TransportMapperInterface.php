<?php

namespace App\Mapper;

use App\Model\TransportInterface;

/**
 * Interface TransportMapperInterface
 *
 * @package App\Mapper
 */
interface TransportMapperInterface
{
    /**
     * @param array $data
     *
     * @return TransportInterface|null
     */
    public function map(array $data): ?TransportInterface;
}
