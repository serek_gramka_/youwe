<?php

namespace App\Controller;

/**
 * Interface ControllerInterface
 *
 * @package App\Controller
 */
interface ControllerInterface
{
    /**
     * @return mixed
     */
    public function indexAction();
}
