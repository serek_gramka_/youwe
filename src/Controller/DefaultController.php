<?php

declare(strict_types=1);

namespace App\Controller;

use App\Loader\DataLoader;
use App\Sorter\CardsSorter;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController implements ControllerInterface
{
    /** @var DataLoader $dataLoader */
    private $dataLoader;

    /** @var CardsSorter $cardsSorter */
    private $cardsSorter;

    /**
     * @param DataLoader $dataLoader
     */
    public function setDataLoader(DataLoader $dataLoader)
    {
        $this->dataLoader = $dataLoader;
    }

    /**
     * @param CardsSorter $cardsSorter
     */
    public function setCardSorter(CardsSorter $cardsSorter)
    {
        $this->cardsSorter = $cardsSorter;
    }

    /**
     * Main method.
     */
    public function indexAction()
    {
        $unsortedData = $this->dataLoader->getData();
        $sortedData = $this->cardsSorter->sort($unsortedData);

        return $this->dumpResults($unsortedData, $sortedData);
    }

    /**
     * @param array $unsortedData
     * @param array $sortedData
     */
    private function dumpResults(array $unsortedData, array $sortedData)
    {
        dump('Unsorted data:', $unsortedData, 'Sorted data:', $sortedData);
    }
}
