<?php

declare(strict_types=1);

namespace App\Sorter;

use App\Model\BoardingCard;
use App\Sorter\Strategy\SorterStrategyInterface;

/**
 * Class CardsSorter
 *
 * @package App\Sorter
 */
class CardsSorter implements SorterInterface
{
    /**
     * @var SorterStrategyInterface $sorterStrategy
     */
    private $sorterStrategy;

    /**
     * CardsSorter constructor.
     *
     * @param SorterStrategyInterface $sorterStrategy
     */
    public function __construct(SorterStrategyInterface $sorterStrategy)
    {
        $this->sorterStrategy = $sorterStrategy;
    }

    /**
     * @param BoardingCard[] $cards
     *
     * @return array
     */
    public function sort(array $cards): array
    {
        return $this->sorterStrategy->getSortedData($cards);
    }
}
