<?php

declare(strict_types=1);

namespace App\Sorter\Strategy;

use App\Model\BoardingCardInterface;

/**
 * Class ChainSorterStrategy
 *
 * @package App\Sorter\Strategy
 */
class ChainSorterStrategy implements SorterStrategyInterface
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function getSortedData(array $data): array
    {
        $first = $this->getFirstCard($data);
        $result[] = $first;

        return $this->buildResultedArray($first, $data, $result);
    }

    /**
     * @param BoardingCardInterface[] $cards
     *
     * @return BoardingCardInterface
     */
    private function getFirstCard(array &$cards): BoardingCardInterface
    {
        /** @var BoardingCardInterface $card */
        foreach ($cards as $card) {
            $fromCity = $card->getFromCity();

            $uniqueMatchesCount = count(array_unique(array_map(function ($card) use ($fromCity) {
                /** @var BoardingCardInterface $card */
                return ($card->getToCity() === $fromCity);
            }, $cards)));

            if ($uniqueMatchesCount === 1) {
                unset($cards[$card->getUid()]);

                return $card;
            }
        }
    }

    /**
     * @param BoardingCardInterface $previous
     * @param array $array
     * @param $result
     *
     * @return array
     */
    private function buildResultedArray(
        BoardingCardInterface $previous,
        array $array,
        &$result
    ): array {
        /** @var BoardingCardInterface $card */
        foreach ($array as $card) {
            if ($previous->getToCity() === $card->getFromCity()) {
                $result[] = $card;
                unset($array[$card->getUid()]);

                $this->buildResultedArray($card, $array, $result);
            }
        }

        return $result;
    }
}
