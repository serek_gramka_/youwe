<?php

namespace App\Sorter\Strategy;

/**
 * Interface SorterStrategyInterface
 *
 * @package App\Sorter\Strategy
 */
interface SorterStrategyInterface
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function getSortedData(array $data): array ;
}
