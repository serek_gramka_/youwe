<?php

namespace App\Sorter;

/**
 * Interface SorterInterface
 *
 * @package App\Sorter
 */
interface SorterInterface
{
    /**
     * @param array $cards
     *
     * @return array
     */
    public function sort(array $cards): array;
}
