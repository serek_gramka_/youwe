<?php

namespace App\Test;

use App\Model\BoardingCard;
use App\Model\BoardingCardInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class BoardingCardTest
 *
 * @package App\Test
 */
class BoardingCardTest extends TestCase
{
    /** @var MockObject $boardingCard */
    public $boardingCard;

    public function setUp()
    {
        $this->boardingCard = $this->createMock(BoardingCard::class);
    }

    public function testInit()
    {
        $this->assertInstanceOf(BoardingCardInterface::class, $this->boardingCard->init());
    }
}
