<?php

namespace App\Test;

use App\Model\TransportTypes;

/**
 * Class TransportTypesTest
 *
 * @package App\Test
 */
class TransportTypesTest
{
    public function testConstExist()
    {
        $mailer = new \ReflectionClass(TransportTypes::class);
        $this->assertArrayHasKey('TYPES', $mailer->getConstants());
        $this->assertArrayHasKey('NUMBER_KEY', $mailer->getConstants());
    }
}
