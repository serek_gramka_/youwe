<?php

namespace App\Test;

use App\Sorter\CardsSorter;
use App\Sorter\Strategy\ChainSorterStrategy;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CardSorterTest extends TestCase
{
    /** @var string $filePath */
    public $filePath = __DIR__ . '/../config/tickets.json';

    /** @var MockObject $sorterStrategy */
    public $sorterStrategy;

    /** @var array $data */
    public $data;

    /** @var array $data */
    public $shuffledData;

    public function setUp()
    {
        $this->sorterStrategy = $this->createMock(ChainSorterStrategy::class);
        $this->data = json_decode(file_get_contents($this->filePath), true);
        $this->shuffledData = $this->data;
        shuffle($this->shuffledData);
    }

    public function testSort()
    {
        $cardSorter = new CardsSorter($this->sorterStrategy);
        $sortedData = $cardSorter->sort($this->shuffledData);
        $this->assertTrue($this->arraysAreSimilar($sortedData, $this->data));
    }

    /**
     * @param array $a
     * @param array $b
     *
     * @return bool
     */
    private function arraysAreSimilar(array $a, array $b)
    {
        if (count(array_diff_assoc($a, $b))) {
            return false;
        }

        foreach($a as $k => $v) {
            if ($v !== $b[$k]) {
                return false;
            }
        }

        return true;
    }
}
