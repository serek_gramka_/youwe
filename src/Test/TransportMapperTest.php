<?php

namespace App\Test;

use App\Mapper\TransportMapper;
use App\Model\TransportInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class TransportMapperTest
 *
 * @package App\Test
 */
class TransportMapperTest extends TestCase
{
    /** @var string $filePath */
    public $filePath = __DIR__ . '/../config/tickets.json';

    /** @var array $data */
    public $data;

    public function setUp()
    {
        $this->data = json_decode(file_get_contents($this->filePath), true);
    }

    public function testMapTransport()
    {
        $transportMapper = new TransportMapper();
        $result = $transportMapper->map($this->data['first']['transport']);

        $this->assertInstanceOf(TransportInterface::class, $result);
    }

    public function testTypeInArray()
    {
        $this->assertArrayHasKey('type', $this->data['first']['transport']);
    }
}
