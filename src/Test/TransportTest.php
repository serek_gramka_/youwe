<?php

namespace App\Test;

use App\Model\Transport;
use PHPUnit\Framework\TestCase;

/**
 * Class TransportTest
 * @package App\Test
 */
class TransportTest extends TestCase
{
    public function testNumberSuccess()
    {
        $transport = new Transport('name');
        $this->assertIsString($transport->getNumber());
        $this->assertEquals('name', $transport->getNumber());
    }

    public function testNumberFailure()
    {
        $transport = new Transport('name');
        $this->assertIsString($transport->getNumber());
        $this->assertNotEquals('name2', $transport->getNumber());
    }
}
