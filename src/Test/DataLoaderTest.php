<?php

namespace App\Test;

use App\Loader\Strategy\JsonLoaderStrategyLoader;
use App\Mapper\TransportMapper;
use App\Model\BoardingCard;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class DataLoaderTest
 *
 * @package App\Tests
 */
class DataLoaderTest extends TestCase
{
    /** @var MockObject $transportMapper */
    public $transportMapper;

    /** @var MockObject $strategyLoader */
    public $strategyLoader;

    /** @var MockObject $boardingCard */
    public $boardingCard;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->transportMapper = $this->createMock(TransportMapper::class);
        $this->strategyLoader = $this->createMock(JsonLoaderStrategyLoader::class);
        $this->boardingCard = $this->createMock(BoardingCard::class);
    }

    // here is should be your test cases for getData method

    /**
     * {@inheritdoc}
     */
    public function tearDown()
    {
        \Mockery::close();
        gc_collect_cycles();
    }

    public function testGetDataSuccess()
    {
        $this->assertTrue(true);
    }
}
