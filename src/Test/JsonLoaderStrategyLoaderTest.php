<?php

namespace App\Test;

use PHPUnit\Framework\TestCase;

/**
 * Class JsonLoaderStrategyLoaderTest
 *
 * @package App\Tests
 */
class JsonLoaderStrategyLoaderTest extends TestCase
{
    public $filePath = __DIR__ . '/../config/tickets.json';

    public function testGetFilePathSuccess()
    {
        $this->assertTrue(is_string($this->filePath));
    }

    public function testFileExist()
    {
        $this->assertTrue(file_exists($this->filePath));
    }

    public function testFileGetContent()
    {
        $data = json_decode(file_get_contents($this->filePath), true);

        $this->assertArrayHasKey('first', $data);
        $this->assertArrayHasKey('second', $data);
    }
}
