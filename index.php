<?php

require_once './vendor/autoload.php';

use \App\Controller\DefaultController;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/** @var ContainerBuilder $containerBuilder */
$containerBuilder = new ContainerBuilder();
/** @var YamlFileLoader $loader */
$loader = new YamlFileLoader($containerBuilder, new FileLocator(__DIR__ . '/src/config/'));
$loader->load('services.yaml');

/** @var DefaultController $defaultController */
$defaultController = $containerBuilder->get('app.default_controller');
$defaultController->indexAction();